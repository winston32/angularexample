import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-link3',
  templateUrl: './link3.component.html',
  styleUrls: ['./link3.component.css']
})
export class Link3Component implements OnInit {
 @Input()
  link2emp : any;

  @Output()
  emitValue  = new EventEmitter<string>();
//  emp : any[] = [];

  constructor() { 

  }

  generateEvent(){
      this.emitValue.emit('my value');
  }

  ngOnInit() {

  //    this.emp = [
  //   {
  //     name:"xyz",
  //     id:'001'
  //   },{
  //     name:"abc",
  //     id:"002"
  //   }
  //   ]
  // }

  }
}
