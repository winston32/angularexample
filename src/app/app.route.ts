import { Route, RouterModule } from '@angular/Router';
import { ModuleWithProviders } from '@angular/core';
import { Link1Component } from './link1/link1.component';
import { Link2Component } from './link2/link2.component';
import { Link3Component } from './link3/link3.component';
import { Link4Component } from './link4/link4.component';
import { Link5Component } from './link5/link5.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';
import { ProjectViewComponent } from './project-view/project-view.component';
import { ProjectClassicalViewComponent } from './project-classical-view/project-classical-view.component';

const myRoutes: Route[] = [
  {
    path: "link2",
    component: Link2Component
  },

  {
    path: "link3",
    component: Link3Component
  },

  {
    path: "link4",
    component: Link4Component
  },

  {
    path: "link5",
    component: Link5Component
  },
  {
      path : "home",
      component : HomeComponent,
      data : {'message' : "hello home"}
  },
  {
    path: "",
    redirectTo : "/home",
    pathMatch : "full"
  },
  {
      path : 'project',
      children : [
           {
            path : '',
            redirectTo : 'dashboard',
            pathMatch : "full"
          },
           {
              path : 'dashboard',
              component : ProjectDashboardComponent,
          },
          {
            path : 'normalview',
            component : ProjectViewComponent
          },
          {
            path : 'classicalview',
            component : ProjectClassicalViewComponent
          }
      ]
  },
  {
      path : "**",
      component :PageNotFoundComponent
  }
];

export const routes : ModuleWithProviders = RouterModule.forRoot(myRoutes);