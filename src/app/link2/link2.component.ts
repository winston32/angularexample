import { Component, OnInit, Input } from '@angular/core';
import {User}  from '../models/User';

@Component({
  selector: 'app-link2',
  templateUrl: './link2.component.html',
  styleUrls: ['./link2.component.css']
})
export class Link2Component implements OnInit {

somedata  =  "some data"
 
  users : any[] = [{
    'username' : 'winston',
    'userId' : '32'
  },
  {
    'username' : 'devagi',
    'userId' : '14'
  }]
  constructor() { }

  ngOnInit() {
    
  }


  showClickedValue() {
    console.log('button clicked');
  }

  showEnteredValue(val:any) {
    console.log(val);
  }

  takeValue(data:any) {
    console.log(data);
  }

}
