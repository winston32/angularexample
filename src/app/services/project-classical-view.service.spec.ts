/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProjectClassicalViewService } from './project-classical-view.service';

describe('ProjectClassicalViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectClassicalViewService]
    });
  });

  it('should ...', inject([ProjectClassicalViewService], (service: ProjectClassicalViewService) => {
    expect(service).toBeTruthy();
  }));
});
