import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/RX';
import {User} from '../models/User';
@Injectable()
export class UserService {
   usersHardCode = [
    {'username' : 'Bittu',
  'userId' : '14'},
   {'username' : 'Smiley',
  'userId' : '16'}
  ]


  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });
  users : User[];

  constructor(private http:Http) { }

  getAllUsers() {
      return this.usersHardCode;
  }

  getAllUsersFromNodeWithObservable() : Observable<User[]> {
       return this.http.get('http://localhost:8080/')
                  .map((data : Response )=> {
                          console.log('data from server for get project IDS from the resource details is : ' + data.json());
                          return data.json() as User[];
                        });
  }

  getAllUsersFromNodeWithpromise() : Promise<User[]> {
    return this.http.get('http://localhost:1337/datatong2')
               .toPromise()
               .then(response => response.json() as User[])
               .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
}
}
