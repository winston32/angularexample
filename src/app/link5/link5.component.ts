import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import {User} from '../models/User';
@Component({
  selector: 'app-link5',
  templateUrl: './link5.component.html',
  styleUrls: ['./link5.component.css'],
  providers : [UserService]
})
export class Link5Component implements OnInit {

  users : any[];
  usersFromNode : User[];
  constructor(private userService:UserService) { }
  
  ngOnInit() {
  //  this.usersFromNode = this.userService.getAllUsers();

    this.userService.getAllUsersFromNodeWithObservable().subscribe((response) => {
                      console.log('response is : ' + response);
                      this.usersFromNode = response;
                    });

    // this.userService.getAllUsersFromNodeWithpromise()
    //                 .then(response => this.usersFromNode = response);
  }
}
