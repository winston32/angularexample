import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-link4',
  templateUrl: './link4.component.html',
  styleUrls: ['./link4.component.css']
})
export class Link4Component implements OnInit {

  constructor() { }

  agree : number = 0;
  disagree : number = 0;

  dataEvent : number;

  ngOnInit() {
  }

  captureVote(data:any) {
    console.log('data from voter componet is : ' + data);
    if(data ==  1) {
      console.log('inside yes');
      this.dataEvent =  Number.parseInt(data);
      this.agree = this.dataEvent ++;

    } 
    if(data ==  0) { 
     this.dataEvent =  Number.parseInt(data);
      this.disagree = this.dataEvent ++;
    }
  }

}
