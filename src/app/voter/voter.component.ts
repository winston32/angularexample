import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-voter',
  templateUrl: './voter.component.html',
  styleUrls: ['./voter.component.css']
})
export class VoterComponent implements OnInit {

  constructor() { }

@Output()
vote = new EventEmitter<number>();
  ngOnInit() {
  }

  generateVote(data:number) {
    console.log('voter value is : ' + data)
    this.vote.emit(data);
  }

}
