import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { Link1Component } from './link1/link1.component';
import { Link2Component } from './link2/link2.component';
import { Link3Component } from './link3/link3.component';
import { Link4Component } from './link4/link4.component';
import { Link5Component } from './link5/link5.component';
import { VoterComponent } from './voter/voter.component';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';
import { ProjectViewComponent } from './project-view/project-view.component';
import { ProjectClassicalViewComponent } from './project-classical-view/project-classical-view.component';
import { routes } from './app.route';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';

const linkPages = [
  Link1Component,
  Link2Component,
  Link3Component,
  Link4Component,
  Link5Component
]

const projectpages = [
  ProjectDashboardComponent,
  ProjectViewComponent,
  ProjectClassicalViewComponent

]

const voterPages = [
  VoterComponent
]




@NgModule({
  declarations: [
    ...projectpages,
    ...voterPages,
    ...linkPages,
    AppComponent,
    AppHeaderComponent,
    AppFooterComponent,
    PageNotFoundComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
